import java.util.Scanner;

public class Activitat8 {
    public Activitat8() {
    }

    public static void main(String[] args) {
        double PI = 3.14159D;
        Scanner tec = new Scanner(System.in);
        System.out.println("Introduce el diametro: ");
        double diametro = tec.nextDouble();
        double radio = diametro / 2.0D;
        System.out.println("Introduce la altura: ");
        double altura = tec.nextDouble();
        double area = 6.28318D * radio * 2.0D + 6.28318D * radio * radio;
        System.out.println("La area es " + area + ".");
        double volumen = 3.14159D * radio * 2.0D * altura;
        System.out.println("El volumen es " + volumen + ".");
    }
}