import java.util.Scanner;

public class Activitat12 {
    public Activitat12() {
    }

    public static void main(String[] args) {


        Scanner tec = new Scanner(System.in);
        System.out.println("Introduce el numero de tres digitos: ");
        int num = tec.nextInt();
        if (num < 1000 && num > 99) {
            System.out.print(num + " = " + num / 100 + "*100 + ");
            num %= 100;
            System.out.println(num / 10 + "*10 + " + num % 10 + ";");
        } else {
            System.out.println("No a lugar...jijiji");
        }

    }
}