
import java.util.Scanner;
public class Activitat9 {
    public Activitat9() {
    }

    public static void main(String[] args) {
        Scanner tec = new Scanner(System.in);
        System.out.println("Introduce una cantidad de segundos: ");
        int segons = tec.nextInt();
        if (segons >= 86400) {
            int dias = segons / 86400;
            segons %= 86400;
            System.out.println(" " + dias + " dias");
        }

        if (segons >= 3600) {
            int horas = segons / 3600;
            segons %= 3600;
            System.out.println(" " + horas + " horas");
        }

        if (segons >= 60) {
            int minutos = segons / 60;
            segons %= 60;
            System.out.println(" " + minutos + " minutos");
        }

        if (segons > 0) {
            System.out.println(" " + segons + " segundos");
        }

    }
}