public class Activitat5 {

    public static void main(String[] args) {
        double ingresado = 2000.0D;
        double interesAnual = 2.75D;
        double retencio = 18.0D;
        double interesSimestre = interesAnual / 2.0D;
        double beneSimestre = ingresado * (interesSimestre / 100.0D);
        System.out.println("Ganancia bruta medio año: " + beneSimestre);
        double cantidadRetenida = beneSimestre * (retencio / 100.0D);
        System.out.println("Cantidad retenida medio año: " + cantidadRetenida);
    }
}
