import java.util.Scanner;

public class Activitat13 {

    public static void main(String[] args) {
        
        Scanner tec = new Scanner(System.in);
        System.out.println("Por favor introduzca la cordenada x del primer punto: ");
        double x1 = tec.nextDouble();
        System.out.println("Por favor introduzca la cordenada y del primer punto: ");
        double y1 = tec.nextDouble();
        System.out.println("Por favor introduzca la cordenada x del segundo punto: ");
        double x2 = tec.nextDouble();
        System.out.println("Por favor introduzca la cordenada y del segundo punto: ");
        double y2 = tec.nextDouble();
        double distancia = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        System.out.printf("La distancia entre los puntos es: %.2f", distancia);
    }
}
