import java.util.Scanner;

public class Activitat11 {
    public Activitat11() {
    }

    public static void main(String[] args) {
        Scanner tec = new Scanner(System.in);
        System.out.println("Ingrese valores ax,bx,c separados por ',': ");
        String valores = tec.nextLine();
        String[] numeros = valores.split(",");
        int a = Integer.parseInt(numeros[0]);
        int b = Integer.parseInt(numeros[1]);
        int c = Integer.parseInt(numeros[2]);
        double x1 = ((double)(-b) + Math.sqrt((double)(b * b - 4 * a * c))) / (double)(2 * a);
        double x2 = ((double)(-b) - Math.sqrt((double)(b * b - 4 * a * c))) / (double)(2 * a);
        System.out.println("La solucion de x1: " + x1);
        System.out.println("La solucion de x2: " + x2);
    }
}