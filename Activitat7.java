import java.util.Scanner;

public class Activitat7 {
    public Activitat7() {
    }

    public static void main(String[] args) {
        Scanner tec = new Scanner(System.in);
        System.out.println("Introduce el ancho: ");
        double ancho = (double)tec.nextInt();
        System.out.printf("Introduce el alto: ");
        double alto = (double)tec.nextInt();
        double area = alto * ancho;
        System.out.println("La area es " + area + ".");
    }
}