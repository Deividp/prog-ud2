public class Activitat4 {

    public static void main(String[] args) {
        double deportivesPreu = 85.0D;
        double desconte = 0.15D;
        double preuFinal = deportivesPreu * (1.0D - desconte);
        System.out.printf(preuFinal + "€");
    }
}
